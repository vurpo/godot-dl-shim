extern crate bindgen;

use std::io::prelude::*;
use std::fs::File;

fn main() {
  let bindings = bindgen::Builder::new("godot_c.h");
  let generated_bindings = bindings.generate().expect("Could not generate Godot bindings!");
  let mut file = File::create("src/godot_c.rs").expect("Failed to open file");
  file.write(format!("pub mod {} {{\n", "godot_c").as_bytes()).unwrap();
  file.write(generated_bindings.to_string().as_bytes()).unwrap();
  file.write(b"}").unwrap();
}
