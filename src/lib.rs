#![feature(proc_macro)]

extern crate libloading;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;

use std::fs::File;
use std::io::prelude::*;
use std::ffi::{
  CString,
  CStr,
};

include!("godot_c.rs");

#[derive(Deserialize, Debug)]
struct GodotDLScript {
  filename: String,
  classes: Vec<GodotClass>,
}

#[derive(Deserialize, Debug)]
struct GodotClass {
  name: String,
  extends: String,
  instance_function: String,
  free_function: String,
  functions: Vec<GodotFunction>,
}

#[derive(Deserialize, Debug)]
struct GodotFunction {
  godot_name: String,
  name: String,
}

#[no_mangle]
pub unsafe extern "C" fn godot_dlscript_init() {
  println!("DLScript shim: DLScript shim started");

  let dlscript_structure = {
    let mut structure_file = File::open(&"dlscript.json").unwrap();
    let mut structure_json = String::new();
    structure_file.read_to_string(&mut structure_json).unwrap();
    serde_json::from_str::<GodotDLScript>(structure_json.as_str()).unwrap()
  };

  println!("DLScript shim: DLScript structure: {:?}", dlscript_structure);

  let dlscript_shared_object = libloading::Library::new(dlscript_structure.filename).unwrap();
    
  for class in dlscript_structure.classes {
    println!("DLScript shim: registering class {}", class.name);
    //let class_name = CString::new(class.name).unwrap().as_ptr() as *const _;
    //let class_extends = CString::new(class.extends).unwrap().as_ptr() as *const _;
    let class_instance_function: godot_c::godot_script_instance_func = {
      match class.instance_function.as_str() {
        "" => None,
        function_name => {
          Some(*dlscript_shared_object.get(function_name.as_bytes()).unwrap())
        }
      }
    };
    let class_free_function: godot_c::godot_script_free_func = {
      match class.free_function.as_str() {
        "" => None,
        function_name => {
          Some(*dlscript_shared_object.get(function_name.as_bytes()).unwrap())
        }
      }
    };
    godot_c::godot_script_register(CString::new(class.extends).unwrap().as_ptr() as *const _, CString::new(class.name.clone()).unwrap().as_ptr() as *const _, class_instance_function, class_free_function);
    
    for function in class.functions {
      println!("DLScript shim: registering function {} for class {}", function.godot_name, class.name);
      let function_ptr = *dlscript_shared_object.get(function.name.as_bytes()).unwrap();
      godot_c::godot_script_add_function(CString::new(class.name.clone()).unwrap().as_ptr() as *const _, CString::new(function.godot_name).unwrap().as_ptr() as *const _, function_ptr);
    }
    
  } 
  std::mem::forget(dlscript_shared_object); //never drop the dynamically loaded library
}
