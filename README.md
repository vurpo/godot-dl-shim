This is a proof of concept of a "shim" DLScript library for Godot, which dynamically reads a list of classes and their functions from a JSON file, and registers those classes and functions to Godot.

This allows for cleaner actual game logic code in any language which doesn't support reflection, by keeping the class/function registration logic separate from the actual game logic. The JSON file can be automatically generated (though I haven't written any such generator atm).

`dlscript.json` contains an example JSON file, the stucture should be pretty clear.

Only compiles on nightly Rust, but it will work on stable from 1.15 onwards. (because it uses Serde)
